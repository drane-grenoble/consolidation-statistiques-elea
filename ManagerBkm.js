function getUAIValues() {
    const uaiValues = [];

    // Sélectionner toutes les lignes du tableau dans le corps (tbody)
    const rows = document.querySelectorAll('#school-list tbody tr');

    rows.forEach(row => {
        // Sélectionner la cellule de la colonne UAI (3ème colonne)
        const uaiCell = row.querySelector('td.text-center:nth-child(3)');

        // Si la cellule existe, récupérer son contenu et l'ajouter au tableau
        if (uaiCell) {
            uaiValues.push(uaiCell.textContent.trim());
        }
    });

    return uaiValues;
}
var domaine = window.location.hostname;
var plateforme=domaine.split(".")[0];

    getUAIValues().forEach(url => {

      window.open('https://'+plateforme+'.elea.apps.education.fr/local/platformagent/index.php?way=school.modal.export&uai='+url.toLowerCase()+'&type=weekly&sesskey=${M.cfg.sesskey}', "_blank");
    });
